# backend

## 로컬로 혼자 돌릴 때

```bash
# ./express-react-demo/backend

npm start
```

## 도커 혼자서 돌릴 때

```bash
# ./express-react-demo/backend
docker build --rm -t backend_test .

docker run --rm  -p 3030:3030 -it backend_test
```

## docker-compose.yml로 돌릴 때

```bash
# ./express-react-demo
docker-compose up -d

docker-compose up
```

## env 파일 설정

### `.env`

로컬에서 실행할 때 사용됨

reload를 하면서 개발해야할 경우 이 파일을 통해 설정

### `.env.docker`

`Dockerfile`/`docker-compose.yml`를 통해서 앱을 실행할 때 사용됨

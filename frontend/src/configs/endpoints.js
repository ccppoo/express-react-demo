
class _Endpoint {}

_Endpoint.prototype.API_scheme = process.env.API_scheme || "http"

_Endpoint.prototype.API_host = process.env.REACT__APP__API_host || "localhost"

_Endpoint.prototype.API_port = process.env.REACT_APP_API_port

_Endpoint.prototype.get_end = function(path){
    let base = `${_Endpoint.prototype.API_scheme}://${_Endpoint.prototype.API_host}`
    let full_url = _Endpoint.prototype.API_port === undefined ? base :`${base}:${_Endpoint.prototype.API_port}`;
    console.log(full_url)
    return full_url
}

const Endpoint = new _Endpoint()

export default Endpoint